import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

class SearchBar extends Component {
    constructor(props){
        super(props);

        this.state = {term: ''};

        //take the existing function, bind it with this and reset function definition.
        this.onInputChange = this.onInputChange.bind(this)
        this.onFormSubmit = this.onFormSubmit.bind(this)
    }

    onInputChange(event){
        this.setState({term: event.target.value});
    }

    onFormSubmit(event){
        event.preventDefault();

        // we need to go and fetch the weather data
        this.props.fetchWeather(this.state.term);
        this.setState({term: ''});
    }

    render(){
        return (
          <form onSubmit={this.onFormSubmit} className="input-group">
              <input
                placeholder="Get a five-day forecast in your favorite cities"
                className="form-control"
                value={this.state.term}
                onChange={this.onInputChange}
              />
              <span className="input-group-btn">
                  <button type="submit" className="btn btn-secondary">Submit</button>
              </span>
          </form>
        );
    }
}

//for ability to call an action. this will notyfing all reducers.
//basiclly register a function to be bind to all reducers.
function mapDispatchToProps(dispatch){
    return bindActionCreators({ fetchWeather }, dispatch);
}

//passing null because we don't need any App state, we only activating action.
export default connect(null,mapDispatchToProps)(SearchBar);